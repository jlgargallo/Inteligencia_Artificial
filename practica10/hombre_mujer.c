#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define epoca 1000
#define K 0.5f

//Funcion de Entrenamiento Perceptron
//Caracteristicas
//1.- Barba
//2.- Vestido
//3.- Falda
//4.- Cabello Largo
//5.- Olor Bonito
//6.- Tacones
//7.- Bello Capilar

float EntNtCaracteristicas(float, float, float, float, float, float, float, float);

//Funcion para las salidas 
float InitNt(float, float);
//Sigmoide
float sigmoide(float);
//pesos aleatorios
void pesos_initNt();

float PesosCaracteristicas[2];	
float biasCaracteristicas=0.5f;

 
float EntNtCaracteristicas( float x0, float x1, float x2, float x3, float x4, float x5, float x6, float target )
{
  float net = 0;
  float out = 0;
  float delta[2];  //Es la variacion de los pesos sinapticos
  float Error;
   
  net = PesosCaracteristicas[0]*x0 + PesosCaracteristicas[1]*x1-biasCaracteristicas;
  net = sigmoide( net );
   
  Error = target - net;
   
  biasCaracteristicas -= K*Error;  //Como el bias es siempre 1, pongo que 
                    //el bias incluye ya su peso sinaptico
   
  delta[0] = K*Error * x0;  //la variacion de los pesos sinapticos corresponde 
  delta[1] = K*Error * x1;  //al error cometido, por la entrada correspondiente
   
  PesosCaracteristicas[0] += delta[0];  //Se ajustan los nuevos valores
  PesosCaracteristicas[1] += delta[1];  //de los pesos sinapticos
   
  out=net;
  return out;
}
 
float InitNt( float x0, float x1 )
{
  float net = 0;
  float out = 0;
   
  net = 0.6 * x0 + 0.6 * x1;
  net=sigmoide( net );
   
  out=net;
  return out;
}
 
float sigmoide( float s ){
  return (1/(1+ exp(-1*s)));
}

void pesos_initNt(void)
{
int i;
  for(  i = 0; i < 2; i++ )
  {
    PesosCaracteristicas[i] = (float)rand()/RAND_MAX;
  }
}

int main(){
  int i=0;
  float aprMujer;
  float aprHombre;
  pesos_initNt();
  while(i<epoca){
   i++;    
    printf("Salida Entrenamiento\n");
    aprMujer=EntNtCaracteristicas(0,1,1,1,1,1,0,1); //el 1 indica que cuenta con esa caracteristica y el 0 que no
    printf("0,1,1,1,1,1,0=%f\n",aprMujer);			// si el resultado se a cerca 1 es mujer
    printf("\n"); 
    aprHombre=EntNtCaracteristicas(1,0,0,0,0,0,1,0);
    printf("1,0,0,0,0,0,1=%f\n",aprHombre);		// si el resultado se acerca a 0 es hombre
    printf("\n"); 
    
    
    printf("Pesos de cada epoca\n");
    printf("Peso 1 = %f\n", PesosCaracteristicas[0]);
    printf("Peso 2 = %f\n", PesosCaracteristicas[1]);
    printf("Bias");
    printf("Bias = %f\n",biasCaracteristicas);
    printf("Resultados\n");
    aprMujer=InitNt(1,1);
    printf("1,1=%f\n",aprMujer);


}

  return 0;



}
