#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define epoca 1000
#define K 0.5f

//Funcion de Entrenamiento Perceptron
float EntNt(float, float, float, float*, float*);
//Funcion para las salidas 
float InitNt(float, float, float*, float);
//Sigmoide
float sigmoide(float);
//pesos aleatorios
void pesos_initNt();
float comprobar(float);

float pesosAnd[2];
float pesosOr[2];
float pesosXor[2];
float biasAnd=0.5f;
float biasOr=0.5f;
float biasXor=0.5f;

 
float EntNt( float x0, float x1, float target, float* pesos, float* bias ){
  float net = 0;
  float out = 0;
  float delta[2];  //Es la variacion de los pesos sinapticos
  float Error;
   
  net = pesos[0]*x0 + pesos[1]*x1-(*bias);
  net = sigmoide( net );
   
  Error = target - net;
   
  *bias -= K*Error;  //Como el bias es siempre 1, pongo que 
                    //el bias incluye ya su peso sinaptico
   
  delta[0] = K*Error * x0;  //la variacion de los pesos sinapticos corresponde 
  delta[1] = K*Error * x1;  //al error cometido, por la entrada correspondiente
   
  *pesos += delta[0];  //Se ajustan los nuevos valores
  *(pesos+1)	+= delta[1];  //de los pesos sinapticos
  
   
  out=net;
  return out;
}
 
float InitNt( float x0, float x1, float* pesos, float bias )
{
  float net = 0;
  float out = 0;
   
  net = pesos[0]*x0 + pesos[1]*x1-bias;
  net=sigmoide( net );
   
  out=net;
  return out;
}
 
 
void pesos_initNt(void)
{
int i;
  for(  i = 0; i < 2; i++ )
  {
    pesosAnd[i] = (float)rand()/RAND_MAX;
    pesosOr[i] = (float)rand()/RAND_MAX;
    pesosXor[i] = (float)rand()/RAND_MAX;
  }
}
 
float sigmoide( float s ){
  return (1/(1+ exp(-1*s)));
}
 
float comprobar(float value){
	return (value > 0.5) ? 1 :  0;
}


int main(){
  int i=0;
  float aprAnd, aprOr, aprXor;
  pesos_initNt();
  while(i<epoca){
    i++;   	
    
    //entrenamiento
    aprAnd= EntNt(1,1,1, pesosAnd, &biasAnd);
    aprOr= EntNt(1,1,1, pesosOr, &biasOr);
    aprXor= EntNt(comprobar(aprAnd), comprobar(aprOr), 0, pesosXor, &biasXor);    
    
    aprAnd= EntNt(1,0,0, pesosAnd, &biasAnd);
    aprOr= EntNt(1,0,0, pesosOr, &biasOr);
    aprXor= EntNt(comprobar(aprAnd), comprobar(aprOr), 1, pesosXor, &biasXor);  
   
    aprAnd= EntNt(0,1,0, pesosAnd, &biasAnd);
    aprOr= EntNt(0,1,0, pesosOr, &biasOr);
    aprXor= EntNt(comprobar(aprAnd), comprobar(aprOr), 1, pesosXor, &biasXor); 
    
    aprAnd= EntNt(0,0,0, pesosAnd, &biasAnd);
    aprOr= EntNt(0,0,0, pesosOr, &biasOr);
    aprXor= EntNt(comprobar(aprAnd), comprobar(aprOr), 0, pesosXor, &biasXor); 
    
    printf("\nResultados OR-AND_XOR\n");
	aprAnd = InitNt(1, 1, pesosAnd, biasAnd);
	printf("\nResultados AND\n");
	printf("1,1=%f\n",aprAnd);	
    aprOr  = InitNt(1, 1, pesosOr, biasOr);
	printf("\nResultados OR\n");
	printf("1,1=%f\n",aprOr);
	aprXor = InitNt(comprobar(aprAnd), comprobar(aprOr), pesosXor, biasXor);
	printf("\nResultados XOR\n");
	printf("%f,%f=%f\n",aprOr, aprAnd,aprXor);
    
    printf("\nResultados OR-AND_XOR\n");
	aprAnd = InitNt(1, 0, pesosAnd, biasAnd);
	printf("\nResultados AND\n");
	printf("1,1=%f\n",aprAnd);	
    aprOr  = InitNt(1, 0, pesosOr, biasOr);
	printf("\nResultados OR\n");
	printf("1,1=%f\n",aprOr);
	aprXor = InitNt(comprobar(aprAnd), comprobar(aprOr), pesosXor, biasXor);
	printf("\nResultados XOR\n");
	printf("%f,%f=%f\n",aprOr, aprAnd,aprXor);
	
	printf("\nResultados OR-AND_XOR\n");
	aprAnd = InitNt(0, 1, pesosAnd, biasAnd);
	printf("\nResultados AND\n");
	printf("1,1=%f\n",aprAnd);	
    aprOr  = InitNt(0, 1, pesosOr, biasOr);
	printf("\nResultados OR\n");
	printf("1,1=%f\n",aprOr);
	aprXor = InitNt(comprobar(aprAnd), comprobar(aprOr), pesosXor, biasXor);
	printf("\nResultados XOR\n");
	printf("%f,%f=%f\n",aprOr, aprAnd,aprXor);
	
	printf("\nResultados OR-AND_XOR\n");
	aprAnd = InitNt(0, 0, pesosAnd, biasAnd);
	printf("\nResultados AND\n");
	printf("1,1=%f\n",aprAnd);	
    aprOr  = InitNt(0, 0, pesosOr, biasOr);
	printf("\nResultados OR\n");
	printf("1,1=%f\n",aprOr);
	aprXor = InitNt(comprobar(aprAnd), comprobar(aprOr), pesosXor, biasXor);
	printf("\nResultados XOR\n");
	printf("%f,%f=%f\n",aprOr, aprAnd,aprXor);    
    
  }

  return 0;

}
