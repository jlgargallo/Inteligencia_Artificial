/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Recursivo;
/**
 *
 * @author JLGargallo
 */
public class Escenario{
    public String[][] escenario() {
        String[][] matriz = {
            {"|", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "|"},
            {"|", "*", " ", " ", " ", " ", "O", "O", " ", " ", " ", " ", "|"},
            {"|", "O", " ", "O", "O", " ", "O", " ", " ", "O", "O", " ", "|"},
            {"|", "O", " ", "O", " ", "O", " ", "O", " ", "O", " ", " ", "|"},
            {"|", "O", " ", "O", " ", " ", " ", "O", " ", "O", " ", "O", "|"},
            {"|", " ", " ", "O", " ", "O", " ", " ", " ", "O", " ", " ", "|"},
            {"|", " ", "O", "O", " ", " ", "O", "O", "O", " ", "O", " ", "|"},
            {"|", " ", " ", " ", " ", "O", " ", " ", " ", " ", " ", " ", "|"},
            {"|", " ", " ", "O", " ", "O", " ", "O", " ", "O", "O", "O", "|"},
            {"|", "O", " ", " ", " ", "O", " ", " ", "O", " ", " ", " ", "|"},
            {"|", "O", " ", "O", " ", "O", " ", " ", " ", " ", "O", "S", "|"},
            {"|", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-"}};
        return matriz;
        // * --> Indica el personaje donde empieza
        // S --> Muestra la salida del laberinto
        // > --> El camino por donde va pasando el personaje
    }
        
    public int filaasterico(String[][] matriz) {
        int fila = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (matriz[i][j].equals("*")) {
                    fila = i;
                }
            }
        }
        return fila;
    }
    public int columasterico(String[][] matriz) {
        int fila = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (matriz[i][j].equals("*")) {
                    fila = j;
                }
            }
        }
        return fila;
    }
     int [] arreglo= new int[2];
     public int [] posS(String [][]matriz){
         for (int i = 0; i < matriz.length; i++) {
             for (int j = 0; j < matriz[0].length; j++) {
                 if (matriz[i][j].equals("S")) {
                     arreglo[0]=i;
                     arreglo[1]=j;
                     System.out.println(arreglo[0] +" "+ arreglo[1]);
                 }
             }
         }
         return arreglo;
     }
}
