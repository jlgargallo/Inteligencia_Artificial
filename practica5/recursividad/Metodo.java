/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Recursivo;
/**
 *
 * @author JLGargallo
 */
public class Metodo {

    public void imprimirMatriz(String[][] matriz){
        for (int i=0; i<matriz.length; i++){
            for (int j=0; j<matriz[0].length; j++){
                System.out.print(matriz[i][j]+ "  ");
            }
            System.out.println();
        }
    }

    public void Mov(String[][] matriz, int filas, int colum) {
        String labe = "x";
        String direccion = "derecha";
        Escenario x = new Escenario();
        int []arreglo = x.posS(matriz);
        System.out.println("Comienza a moverse");
        while (matriz[filas][colum] != matriz[arreglo[0]][arreglo[1]]) {
            imprimirMatriz(matriz);
            switch (direccion) {
                case "derecha":
                    if(!matriz[filas - 1][colum].equals("O") && !matriz[filas - 1][colum].equals("|") && !matriz[filas-1][colum].equals("-")) {
                        matriz[filas - 1][colum] = "*";
                        matriz[filas][colum] = ">";
                        filas--;
                        direccion = "arriba";
                    } else{
                        if(!matriz[filas][colum + 1].equals("O") && !matriz[filas][colum + 1].equals("|") && !matriz[filas][colum + 1].equals("-")) {
                            matriz[filas][colum + 1] = "*";
                            matriz[filas][colum] = ">";
                            colum++;
                        } else{
                            if(!matriz[filas + 1][colum].equals("O") && !matriz[filas + 1][colum].equals("|") && !matriz[filas+1][colum].equals("-")) {
                                matriz[filas + 1][colum] = "*";
                                matriz[filas][colum] = ">";
                                filas++;
                                direccion = "abajo";
                            } else{
                                matriz[filas][colum - 1] = "*";
                                matriz[filas][colum] = ">";
                                colum--;
                                direccion = "izquierda";
                            }
                        }
                    }
                    break;
                case "arriba":
                    if (!matriz[filas][colum - 1].equals("O") && !matriz[filas][colum - 1].equals("|") && !matriz[filas][colum - 1].equals("-")) {
                        matriz[filas][colum - 1] = "*";
                        matriz[filas][colum] = ">";
                        colum--;
                        direccion = "izquierda";
                    } else {
                        if (!matriz[filas - 1][colum].equals("O") && !matriz[filas - 1][colum].equals("|") && !matriz[filas-1][colum].equals("-")) {
                            matriz[filas - 1][colum] = "*";
                            matriz[filas][colum] = ">";
                            filas--;
                        } else {
                            if (!matriz[filas][colum + 1].equals("O") && !matriz[filas][colum + 1].equals("|") && !matriz[filas][colum + 1].equals("-")) {
                                matriz[filas][colum + 1] = "*";
                                matriz[filas][colum] = ">";
                                colum++;
                                direccion = "derecha";
                            } else {
                                matriz[filas + 1][colum] = "*";
                                matriz[filas][colum] = ">";
                                filas++;
                                direccion = "abajo";
                            }
                        }
                    }
                    break;
                case "izquierda":
                    if (!matriz[filas + 1][colum].equals("O") && !matriz[filas + 1][colum].equals("|") && !matriz[filas+1][colum].equals("-")) {
                        matriz[filas + 1][colum] = "*";
                        matriz[filas][colum] = ">";
                        filas++;
                        direccion = "abajo";
                    } else {
                        if (!matriz[filas][colum - 1].equals("O") && !matriz[filas][colum - 1].equals("|") && !matriz[filas][colum - 1].equals("-")) {
                            matriz[filas][colum - 1] = "*";
                            matriz[filas][colum] = ">";
                            colum--;
                        } else {
                            if (!matriz[filas - 1][colum].equals("O") && !matriz[filas - 1][colum].equals("|") && !matriz[filas-1][colum].equals("-")) {
                                matriz[filas - 1][colum] = "*";
                                matriz[filas][colum] = ">";
                                filas--;
                                direccion = "arriba";
                            } else {
                                matriz[filas][colum + 1] = "*";
                                matriz[filas][colum] = ">";
                                colum++;
                                direccion = "derecha";
                            }
                        }
                    }
                    break;

                case "abajo":
                    if (!matriz[filas][colum + 1].equals("O") && !matriz[filas][colum + 1].equals("|") && !matriz[filas][colum + 1].equals("-")) {
                        matriz[filas][colum + 1] = "*";
                        matriz[filas][colum] = ">";
                        colum++;
                        direccion = "derecha";
                    } else {
                        if (!matriz[filas + 1][colum].equals("O") && !matriz[filas + 1][colum].equals("|") && !matriz[filas+1][colum].equals("-")) {
                            matriz[filas + 1][colum] = "*";
                            matriz[filas][colum] = ">";
                            filas++;
                        } else {
                            if (!matriz[filas][colum - 1].equals("O") && !matriz[filas][colum - 1].equals("|") && !matriz[filas][colum - 1].equals("-")) {
                                matriz[filas][colum - 1] = "*";
                                matriz[filas][colum] = ">";
                                colum--;
                                direccion = "izquierda";
                            } else {
                                matriz[filas - 1][colum] = "*";
                                matriz[filas][colum] = ">";
                                filas--;
                                direccion = "arriba";
                            }
                        }
                    }
                    break;
                default:
                    throw new AssertionError();
            }
            try {
                Thread.sleep(250);
            }
            catch(InterruptedException e) {
                System.out.println("Thread Interrupted");
            } 
        }
        imprimirMatriz(matriz);
        System.out.println("Has Ganado!!");
    }
}
