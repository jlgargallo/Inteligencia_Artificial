/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package laberinto;
/**
 *
 * @author JLGargallo
 */
public class Laberinto {

    static String maze[][]
         = {{"|", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "|"},
            {"|", " ", " ", " ", " ", " ", "O", "O", " ", " ", " ", " ", "|"},
            {"|", "O", " ", "O", "O", " ", "O", " ", " ", "O", "O", " ", "|"},
            {"|", "O", " ", "O", " ", "O", " ", "O", " ", "O", " ", " ", "|"},
            {"|", "O", " ", "O", " ", " ", " ", "O", " ", "O", " ", "O", "|"},
            {"|", " ", " ", "O", " ", "O", " ", " ", " ", "O", " ", " ", "|"},
            {"|", " ", "O", "O", " ", " ", "O", "O", "O", " ", "O", " ", "|"},
            {"|", " ", " ", " ", " ", "O", " ", " ", " ", " ", " ", " ", "|"},
            {"|", " ", " ", "O", " ", "O", " ", "O", " ", "O", "O", "O", "|"},
            {"|", "O", " ", " ", " ", "O", " ", " ", "O", " ", " ", " ", "|"},
            {"|", "O", " ", "O", " ", "O", " ", " ", " ", " ", "O", "S", "|"},
            {"|", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "|"}};

    public static void main(String[] args) {

        mov(1, 1, 0);
    }

    
    
    static String[][] mov(int aux1, int aux2, int aux3) {
        aux3++;
        imprimir();
        if (maze[aux1 + 1][aux2].equals("S") || maze[aux1][aux2 + 1].equals("S") || maze[aux1 - 1][aux2].equals("S") || maze[aux1][aux2 - 1].equals("S")) {
            maze[aux1][aux2] = ">";
            System.out.println("Has Ganado!!");
            imprimir();
            return null;
        } else {
            if (maze[aux1][aux2 + 1].equals(" ")) {
                if (maze[aux1 + 1][aux2].equals(" ")) {
                    System.out.println("1");
                    maze[aux1][aux2] = "D";
                    aux2++;
                    mov(aux1, aux2, aux3);
                } else if (maze[aux1][aux2 - 1].equals(" ")) {
                    maze[aux1][aux2] = "D";
                    System.out.println("2");
                    aux2++;
                    mov(aux1, aux2, aux3);
                } else if (maze[aux1 - 1][aux2].equals(" ")) {
                    maze[aux1][aux2] = "D";
                    System.out.println("3");
                    aux2++;
                    mov(aux1, aux2, aux3);
                } else {
                    maze[aux1][aux2] = ">";
                    System.out.println("4");
                    aux2++;
                    mov(aux1, aux2, aux3);
                }
            } else if (maze[aux1 + 1][aux2].equals(" ")) {
                if (maze[aux1][aux2 - 1].equals(" ")) {
                    System.out.println("5");
                    maze[aux1][aux2] = "D";
                    aux1++;
                    mov(aux1, aux2, aux3);
                } else if (maze[aux1 - 1][aux2].equals(" ")) {
                    maze[aux1][aux2] = "D";
                    System.out.println("6");
                    aux1++;
                    mov(aux1, aux2, aux3);
                } else {
                    maze[aux1][aux2] = ">";
                    System.out.println("7");
                    aux1++;
                    mov(aux1, aux2, aux3);
                }
            } else if (maze[aux1][aux2 - 1].equals(" ")) {
                if (maze[aux1 - 1][aux2].equals(" ")) {
                    maze[aux1][aux2] = "D";
                    System.out.println("8");
                    aux2--;
                    mov(aux1, aux2, aux3);
                } else {
                    maze[aux1][aux2] = ">";
                    System.out.println("9");
                    aux2--;
                    mov(aux1, aux2, aux3);
                }
            } else if (maze[aux1 - 1][aux2].equals(" ")) {
                maze[aux1][aux2] = ">";
                System.out.println("10");
                aux1--;
                mov(aux1, aux2, aux3);
            } else {
                maze[aux1][aux2] = ">";
                dif(aux1, aux2, aux3);
            }
            return maze;
        }
    }

    static void dif(int aux1, int aux2, int aux3) {
        for (int i = 0; i < maze[0].length; i++) {
            for (int j = 0; j < maze.length; j++) {
                if (maze[j][i].equals("D")) {
                    maze[j][i] = ">";
                    aux1 = j;
                    aux2 = i;
                    i = maze[0].length;
                    j = maze.length;
                }
            }
        }
        mov(aux1, aux2, aux3);
    }

    static void imprimir() {
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[i].length; j++) {
                System.out.print(maze[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("");
    }
}
